# All code provided "AS IS".Test properly and use responsibly. -- 2018 Sam Palani

import json
import botocore
import boto3
client = boto3.client('s3')
s3 = boto3.resource('s3')
def lambda_handler(event, context):
    # parsed_event = json.loads(event)
    objects = event['detail']['summary']['Object']
    print (type(objects))
    for x,y in objects.items():
        print(x)
        # find the postion first occurance of '/' to get the bucket name
        position = x.find("/")
        bucket = x[:position]
        print (bucket)
        # increment the position by 1 to remove the preceding '/' from the key name
        position +=1
        key = x[position:]
        print(key)
        new_key = key+".enc"
        print (new_key)
        try:
            s3.Object(bucket, key).load()
        # exception handling for deleted keys
        except botocore.exceptions.ClientError as e:
            if e.response['Error']['Code'] == "404":
                print ("Object already cleaned or deleted")
            else:
                print ("Something else broke, check your cloudwatch logs")
        else:
            # Check if the object is already encrypted to avoid loop *
            response=client.get_object(Bucket=bucket,Key=key)
            if 'ServerSideEncryption' in response:
                print ("Object already encrypted")
            else:
                print("Unencrypted credentials found, moving adhead with encryption and deletion")
                # Make a copy of the object with SSE AES256
                response = client.copy_object(Bucket=bucket,CopySource=x,Key=new_key,ServerSideEncryption='AES256')
                print(response)
                # Finally delete the original object
                response = client.delete_object(Bucket=bucket,Key=key)
                print(response)
    return

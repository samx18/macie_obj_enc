# macie_obj_enc

## Description

Lambda function to encrypt S3 objects that contain a specific Macie theme.

## How it works

The lambda function will first make an encrypted (SSE AES256) copy of the S3 objects flagged by AWS Macie to contain PII or sensitive information matching a theme. After the copy and encryption is successful, the function will delete the original object.
If the object does not exist any longer or is already encrypted, the function will exit peacefully.

The example code looks for the Macie theme `aws_credentails exposed`

## Usage

* Setup cloudwatch rule for Macie with the event pattern matching `src/event_pattern.json`.
* Create a role using the policy `src/s3_enc_policy.json` .
* Create a lamda function using `src/lambda_function.py` with Python 3 runtime environment and attach the above role.
* Create a test event using `test/test_event.json` for testing.
* Add the cloudwatch rule as a trigger for the lambda function created in the preceding step.


